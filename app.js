const express = require("express");
const querystring = require("querystring");
const port = 3000;
const app = express();
const mongoose = require("mongoose");

// List of all messages
let messages = [];// will eventually be an array of objects with fields "sender"
//"timestamp" and "message"

// Track last active times for each sender
let users = {};//have fields "name" and "active"



const db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function () {
    console.log("we're connected!")
});

mongoose.connect("mongodb://localhost:27017/klack", { useNewUrlParser: true, useUnifiedTopology: true });//from Jake

const messageSchema = new mongoose.Schema({ //stack overflow
    sender: String,
    message: String,
    timestamp: Number
});


const userSchema = new mongoose.Schema({ //stack overflow
  name: String,
  active: Boolean
});


const MessageResponse = mongoose.model('MessageResponse', messageSchema);
const UserResponse = mongoose.model('UserResponse', userSchema); //haven't made new


app.use(express.static("./public"));
app.use(express.json());

// generic comparison function for case-insensitive alphabetic sorting on the name field
function userSortFn(a, b) {
  let nameA = a.name.toUpperCase(); // ignore upper and lowercase
  let nameB = b.name.toUpperCase(); // ignore upper and lowercase
  if (nameA < nameB) {
    return -1;
  }
  if (nameA > nameB) {
    return 1;
  }

  // names must be equal
  return 0;
}





app.get("/messages", async(request, response) => {
  // get the current time
  await MessageResponse.find({}, (err, response)=>{
    response.forEach((message)=>{
      messages.push(message)
    })
  })
  
  const now = Date.now();

  // consider users active if they have connected (GET or POST) in last 15 seconds
  const requireActiveSince = now - 15 * 1000;

  // create a new list of users with a flag indicating whether they have been active recently
  usersSimple = Object.keys(users).map(x => ({ //mapping over all user keys, assigning each index a property of name   
    name: x,
    active: users[x] > requireActiveSince
  }));

  // sort the list of users alphabetically by name
  usersSimple.sort(userSortFn);
  usersSimple.filter(a => a.name !== request.query.for);

  // update the requesting user's last access time
  users[request.query.for] = now;
console.log(now)
  // send the latest 40 messages and the full user list, annotated with active flags
 await response.send({ messages: messages.slice(-40), users: usersSimple });
});











app.post("/messages", async(request, response) => {
  // add a timestamp to each incoming message.
  const now = Date.now();

  // consider users active if they have connected (GET or POST) in last 15 seconds
  const requireActiveSince = now - 15 * 1000;
  

  try{
  const timestamp = Date.now()
  const message= await new MessageResponse({
  sender:request.body.sender,
  message:request.body.message,
  timestamp:timestamp
  });
   
  message.save()
 
  const user= await new UserResponse({
    name:request.body.sender,
    active:timestamp > requireActiveSince
  })
 
 user.save()
  // append the new message to the message list
  //messages.push(message);
console.log(message)
console.log(user)
  // update the posting user's last access timestamp (so we know they are active)
  //users[request.body.sender] = timestamp;

  // Send back the successful response.
  response.status(201);
  response.send(request.body);
}
catch(err){
    console.error(err)
    res.status(500).send(err.message)
}
});

app.listen(3000);